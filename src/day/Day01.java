package day;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.stream.Collectors;

public class Day01 {

	public static void main(String[] args) throws FileNotFoundException {

		Scanner sc = new Scanner(new File("src/sources/day01.txt"));
		ArrayList<Integer> totalElfe = new ArrayList<Integer>();
		int elfeEnCours = 0;
		while (sc.hasNextLine()) {
			String line = sc.nextLine();

			if (line.equals("")) {
				totalElfe.add(elfeEnCours);
				elfeEnCours = 0;
			} else {
				elfeEnCours += Integer.valueOf(line);
			}
		}

		System.out.println(partOne(totalElfe));
		System.out.println(partTwo(totalElfe));

	}

	static int partOne(ArrayList<Integer> totalElfe) {

		Integer max = totalElfe
				.stream()
				.mapToInt(v -> v)
				.max().orElseThrow(NoSuchElementException::new);

		return max;
	}

	static int partTwo(ArrayList<Integer> totalElfe) {

		List<Integer> listTrie = totalElfe.stream()
				.sorted(Comparator.reverseOrder())
				.collect(Collectors.toList());

		return listTrie.get(0) + listTrie.get(1) + listTrie.get(2);
	}
}
