package day;

import Utils.Position;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Day09 {

	public static void main(String[] args) throws FileNotFoundException {

		Scanner sc = new Scanner(new File("src/sources/day09.txt"));
		Map<String,String> corde = new HashMap<String,String>();
		Position positionH = new Position(0,0);
		Position positionT = new Position(0,0);

		while (sc.hasNextLine()) {
			String line = sc.nextLine();
			String direction = line.split(" ")[0];
			Integer deplacement = Integer.valueOf(line.split(" ")[1]);
			if(direction.equals("U")){
				for(int i=0;i<deplacement;i++){
					positionH.y++;
					int valAbs = Math.abs(positionT.y-positionH.y);
					if(valAbs>1){
						positionT.y++;
						if(positionT.x != positionH.x) {
							positionT.x = positionH.x;
						}
					}
					if(corde.get(positionT.x+","+positionT.y) == null){
						corde.put(positionT.x+","+positionT.y,"#");
					}
				}
			}else if(direction.equals("D")){
				for(int i=0;i<deplacement;i++){
					positionH.y--;
					int valAbs = Math.abs(positionT.y - positionH.y);
					if (valAbs > 1) {
						positionT.y--;
						if(positionT.x != positionH.x) {
							positionT.x = positionH.x;
						}
					}
					if (corde.get(positionT.x + "," + positionT.y) == null) {
						corde.put(positionT.x + "," + positionT.y, "#");
					}

				}
			}else if(direction.equals("R")){
				for(int i=0;i<deplacement;i++){
					positionH.x++;
					int valAbs = Math.abs(positionT.x-positionH.x);
					if(valAbs>1){
						positionT.x++;
						if(positionT.y != positionH.y) {
							positionT.y = positionH.y;
						}
					}
					if(corde.get(positionT.x+","+positionT.y) == null){
						corde.put(positionT.x+","+positionT.y,"#");
					}
				}
			}else{
				for(int i=0;i<deplacement;i++){
					positionH.x--;
					int valAbs = Math.abs(positionT.x - positionH.x);
					if (valAbs > 1) {
						positionT.x--;
						if(positionT.y != positionH.y) {
							positionT.y = positionH.y;
						}
					}
					if (corde.get(positionT.x + "," + positionT.y) == null) {
						corde.put(positionT.x + "," + positionT.y, "#");
					}
				}
			}
		}

		System.out.println(corde.size());

	}
}