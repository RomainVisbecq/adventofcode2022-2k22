package day;

import Utils.Case;
import Utils.Pair;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Day13 {

	public static void main(String[] args) throws FileNotFoundException {

		Scanner sc = new Scanner(new File("src/sources/day13.txt"));
		Pair pair = new Pair();
		List<Pair> mesPairs = new ArrayList<>();
		int j =0;
		while (sc.hasNextLine()) {
			if(j==0) {
				pair.lineOne = sc.nextLine();
			}else if(j==1){
				pair.lineTwo = sc.nextLine();
			}else{
				sc.nextLine();
				mesPairs.add(pair);
				pair = new Pair();
				j=-1;
			}
			j++;
		}
		mesPairs.add(pair);

		for(Pair testPair : mesPairs){
			int max = (testPair.lineOne.length() > testPair.lineTwo.length()) ? testPair.lineOne.length() : testPair.lineTwo.length();
			int nbList1 = 0;
			int nbList2 = 0;
			Map<Integer,List<Integer>> mapListLeft = new HashMap<>();
			Map<Integer,List<Integer>> mapListRight= new HashMap<>();

			String[] leftTab = testPair.lineOne.split(",");
			for(String leftEl :leftTab){
				int i=0;
				while(i<leftEl.length()){
					String nb = "";
					if(leftEl.charAt(i) == '['){
						nbList1++;
						i++;
					} else if (leftEl.charAt(i) == ']') {
						nbList1--;
						i++;
					} else{
						while(i<leftEl.length() && leftEl.charAt(i) != '[' && leftEl.charAt(i) != ']'){
							nb += leftEl.charAt(i);
							i++;
						}
						if(mapListLeft.get(nbList1) != null){
							mapListLeft.get(nbList1).add(Integer.valueOf(nb));
						}else{
							ArrayList listInt = new ArrayList<>();
							listInt.add(Integer.valueOf(nb));
							mapListLeft.put(nbList1,listInt);
						}
					}
				}
			}

			String[] rightTab = testPair.lineTwo.split(",");
			for(String rightEl :rightTab){
				int i=0;
				while(i<rightEl.length()){
					String nb = "";
					if(rightEl.charAt(i) == '['){
						nbList2++;
						i++;
					} else if (rightEl.charAt(i) == ']') {
						nbList2--;
						i++;
					} else{
						while(i<rightEl.length() && rightEl.charAt(i) != '[' && rightEl.charAt(i) != ']'){
							nb += rightEl.charAt(i);
							i++;
						}
						if(mapListRight.get(nbList2) != null){
							mapListRight.get(nbList2).add(Integer.valueOf(nb));
						}else{
							ArrayList listInt = new ArrayList<>();
							listInt.add(Integer.valueOf(nb));
							mapListRight.put(nbList2,listInt);
						}
					}
				}
			}
		System.out.println(testPair);
		}
	}
}