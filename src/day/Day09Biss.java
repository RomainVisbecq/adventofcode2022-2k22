package day;

import Utils.Position;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Day09Biss {

	public static void main(String[] args) throws FileNotFoundException {

		Scanner sc = new Scanner(new File("src/sources/day09.txt"));
		Map<String,String> corde = new HashMap<String,String>();
		List<Position> lesPositions = new ArrayList<Position>();
		lesPositions.add(new Position(0,0));
		lesPositions.add(new Position(0,0));
		lesPositions.add(new Position(0,0));
		lesPositions.add(new Position(0,0));
		lesPositions.add(new Position(0,0));
		lesPositions.add(new Position(0,0));
		lesPositions.add(new Position(0,0));
		lesPositions.add(new Position(0,0));
		lesPositions.add(new Position(0,0));
		lesPositions.add(new Position(0,0));

		while (sc.hasNextLine()) {
			String line = sc.nextLine();
			String direction = line.split(" ")[0];
			Integer deplacement = Integer.valueOf(line.split(" ")[1]);
				if(direction.equals("U")){
					for(int i=0;i<deplacement;i++){
						lesPositions.get(0).y++;
						for(int j=1;j<lesPositions.size();j++){
							int valAbsY = Math.abs(lesPositions.get(j-1).y-lesPositions.get(j).y);
							if(valAbsY>1){
								lesPositions.get(j).y++;
								if(lesPositions.get(j-1).x < lesPositions.get(j).x) {
									lesPositions.get(j).x--;
								}
								if(lesPositions.get(j-1).x > lesPositions.get(j).x) {
									lesPositions.get(j).x++;
								}
							}
							int valAbsX = Math.abs(lesPositions.get(j-1).x-lesPositions.get(j).x);
							if(valAbsX>1){
								lesPositions.get(j).x++;
								if(lesPositions.get(j-1).y < lesPositions.get(j).y) {
									lesPositions.get(j).y--;
								}
								if(lesPositions.get(j-1).y > lesPositions.get(j).y) {
									lesPositions.get(j).y++;
								}
							}
						}
						if(corde.get(lesPositions.get(lesPositions.size()-1).x+","+lesPositions.get(lesPositions.size()-1).y) == null){
							corde.put(lesPositions.get(lesPositions.size()-1).x+","+lesPositions.get(lesPositions.size()-1).y,"#");
						}
					}
				}else if(direction.equals("D")){
					for(int i=0;i<deplacement;i++){
						lesPositions.get(0).y--;
						for(int j=1;j<lesPositions.size();j++){
							int valAbsY = Math.abs(lesPositions.get(j-1).y-lesPositions.get(j).y);
							if(valAbsY>1){
								lesPositions.get(j).y--;
								if(lesPositions.get(j-1).x < lesPositions.get(j).x) {
									lesPositions.get(j).x--;
								}
								if(lesPositions.get(j-1).x > lesPositions.get(j).x) {
									lesPositions.get(j).x++;
								}
							}
							int valAbsX = Math.abs(lesPositions.get(j-1).x-lesPositions.get(j).x);
							if(valAbsX>1){
								lesPositions.get(j).x--;
								if(lesPositions.get(j-1).y < lesPositions.get(j).y) {
									lesPositions.get(j).y--;
								}
								if(lesPositions.get(j-1).y > lesPositions.get(j).y) {
									lesPositions.get(j).y++;
								}
							}
						}
						if(corde.get(lesPositions.get(lesPositions.size()-1).x+","+lesPositions.get(lesPositions.size()-1).y) == null){
							corde.put(lesPositions.get(lesPositions.size()-1).x+","+lesPositions.get(lesPositions.size()-1).y,"#");
						}
					}
				}else if(direction.equals("R")){
					for(int i=0;i<deplacement;i++){
						lesPositions.get(0).x++;
						for(int j=1;j<lesPositions.size();j++){
							int valAbsX = Math.abs(lesPositions.get(j-1).x-lesPositions.get(j).x);
							if(valAbsX>1){
								lesPositions.get(j).x++;
								if(lesPositions.get(j-1).y < lesPositions.get(j).y) {
									lesPositions.get(j).y--;
								}
								if(lesPositions.get(j-1).y > lesPositions.get(j).y) {
									lesPositions.get(j).y++;
								}
							}
							int valAbsY = Math.abs(lesPositions.get(j-1).y-lesPositions.get(j).y);
							if(valAbsY>1){
								lesPositions.get(j).y++;
								if(lesPositions.get(j-1).x < lesPositions.get(j).x) {
									lesPositions.get(j).x--;
								}
								if(lesPositions.get(j-1).x > lesPositions.get(j).x) {
									lesPositions.get(j).x++;
								}
							}
						}
						if(corde.get(lesPositions.get(lesPositions.size()-1).x+","+lesPositions.get(lesPositions.size()-1).y) == null){
							corde.put(lesPositions.get(lesPositions.size()-1).x+","+lesPositions.get(lesPositions.size()-1).y,"#");
						}
					}
				}else{
					for(int i=0;i<deplacement;i++){
						lesPositions.get(0).x--;
						for(int j=1;j<lesPositions.size();j++){
							int valAbsX = Math.abs(lesPositions.get(j-1).x-lesPositions.get(j).x);
							if(valAbsX>1){
								lesPositions.get(j).x--;
								if(lesPositions.get(j-1).y < lesPositions.get(j).y) {
									lesPositions.get(j).y--;
								}
								if(lesPositions.get(j-1).y > lesPositions.get(j).y) {
									lesPositions.get(j).y++;
								}
							}
							int valAbsY = Math.abs(lesPositions.get(j-1).y-lesPositions.get(j).y);
							if(valAbsY>1){
								lesPositions.get(j).y--;
								if(lesPositions.get(j-1).x < lesPositions.get(j).x) {
									lesPositions.get(j).x--;
								}
								if(lesPositions.get(j-1).x > lesPositions.get(j).x) {
									lesPositions.get(j).x++;
								}
							}
						}
						if(corde.get(lesPositions.get(lesPositions.size()-1).x+","+lesPositions.get(lesPositions.size()-1).y) == null){
							corde.put(lesPositions.get(lesPositions.size()-1).x+","+lesPositions.get(lesPositions.size()-1).y,"#");
						}
					}
				}
		}

		System.out.println(corde.size());

	}
}