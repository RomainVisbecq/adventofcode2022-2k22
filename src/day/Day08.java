package day;

import Utils.Contenu;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Day08 {

	public static void main(String[] args) throws FileNotFoundException {

		Scanner sc = new Scanner(new File("src/sources/day08.txt"));
		Map<String,Integer> foret = new HashMap<String,Integer>();
		int j = 0;
		int retour = 0;
		while (sc.hasNextLine()) {
			String line = sc.nextLine();
			int i=0;
			for(Character arbre: line.toCharArray()){
				foret.put(i+","+j,Integer.valueOf(arbre.toString()));
				i++;
			}
			j++;
		}

		for (Map.Entry<String, Integer> entry : foret.entrySet()) {
//			partOne
//			if(testSi(foret,entry.getKey(),entry.getValue())){
//				retour++;
//			}
// 			partTwo
				int arbre = testSi(foret, entry.getKey(), entry.getValue());
				if (arbre > retour) {
					retour = arbre;
				}
		}

		System.out.println(retour);
	}

	private static int testSi(Map<String, Integer> foret, String arbre,Integer valeur) {
		//return (testSiGauche(foret,arbre,valeur) || testSiDroite(foret,arbre,valeur) || testSiHaut(foret,arbre,valeur) || testSiBas(foret,arbre,valeur));
		return testSiGauche(foret,arbre,valeur) * testSiDroite(foret,arbre,valeur) * testSiHaut(foret,arbre,valeur) * testSiBas(foret,arbre,valeur);
	}

	private static int testSiBas(Map<String, Integer> foret, String arbre, Integer valeur) {
		int x = Integer.valueOf(arbre.split(",")[0]);
		int y = Integer.valueOf(arbre.split(",")[1])+1;
		if(foret.get(x+","+y) != null){
			int valeurTest = foret.get(x+","+y);
			if(valeurTest<valeur){
				//return testSiBas(foret,x+","+y,valeur);
				return 1+testSiBas(foret,x+","+y,valeur);
			}else{
				//return false;
				return 1;
			}
		}else {
			//return true;
			return 0;
		}
	}

	private static int testSiHaut(Map<String, Integer> foret, String arbre, Integer valeur) {
		int x = Integer.valueOf(arbre.split(",")[0]);
		int y = Integer.valueOf(arbre.split(",")[1])-1;
		if(foret.get(x+","+y) != null){
			int valeurTest = foret.get(x+","+y);
			if(valeurTest<valeur){
				//return testSiHaut(foret,x+","+y,valeur);
				return 1+testSiHaut(foret,x+","+y,valeur);
			}else{
				//return false;
				return 1;
			}
		}else {
			//return true;
			return 0;
		}
	}

	private static int testSiDroite(Map<String, Integer> foret, String arbre, Integer valeur) {
		int x= Integer.valueOf(arbre.split(",")[0])+1;
		int y = Integer.valueOf(arbre.split(",")[1]);
		if(foret.get(x+","+y) != null){
			int valeurTest = foret.get(x+","+y);
			if(valeurTest<valeur){
				//return testSiDroite(foret,x+","+y,valeur);
				return 1+testSiDroite(foret,x+","+y,valeur);
			}else{
				//return false;
				return 1;
			}
		}else {
			//return true;
			return 0;
		}
	}

	private static int testSiGauche(Map<String, Integer> foret, String arbre,Integer valeur) {
		int x = Integer.valueOf(arbre.split(",")[0])-1;
		int y = Integer.valueOf(arbre.split(",")[1]);
		if(foret.get(x+","+y) != null){
			int valeurTest = foret.get(x+","+y);
			if(valeurTest<valeur){
				//return testSiGauche(foret,x+","+y,valeur);
				return 1+testSiGauche(foret,x+","+y,valeur);
			}else{
				//return false;
				return 1;
			}
		}else {
			//return true;
			return 0;
		}
	}
}