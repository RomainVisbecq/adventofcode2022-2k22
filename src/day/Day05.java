package day;

import Utils.Pair;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Day05 {

	public static void main(String[] args) throws FileNotFoundException {

		Scanner sc = new Scanner(new File("src/sources/day05.txt"));
		int count1 = 0;
		Map<Integer, Stack> stacks = new HashMap<Integer, Stack>();
		while (sc.hasNextLine()) {
			String line = sc.nextLine();
			if(line.length()>0 && !line.contains("1") && line.charAt(0) != 'm') {
				int j = 0;
				for (int i = 0; i < line.length() - 2; i = i + 4) {
					j++;
					char caisse = line.charAt(i + 1);
					if (caisse != ' ') {
						if (stacks.get(j) == null) {
							Stack stack = new Stack();
							stack.push(caisse);
							stacks.put(j, stack);
						} else {
							stacks.get(j).push(caisse);
						}
					}
				}
			}
			if(line.length()==0){
				for (Map.Entry<Integer, Stack> entry : stacks.entrySet()) {
					Stack newStack = new Stack();
					int oldStackSize = entry.getValue().size();
					for(int i=0;i<oldStackSize;i++){
						newStack.push(entry.getValue().pop());
					}
					entry.setValue(newStack);
				}

			}
			if(line.length()>0 && line.charAt(0) == 'm'){
				int nb = Integer.valueOf(line.split("from")[0].split("move")[1].replaceAll(" ",""));
				int begin = Integer.valueOf(line.split("from")[1].split("to")[0].replaceAll(" ",""));
				int end = Integer.valueOf(line.split("from")[1].split("to")[1].replaceAll(" ",""));

				//movePartOne
		//		for(int i=0;i<nb;i++){
		//			stacks.get(end).push(stacks.get(begin).pop());
		//		}

				//movePartTwo
				Stack tempo = new Stack();
				for(int i=0;i<nb;i++){
					tempo.push(stacks.get(begin).pop());
				}
				for(int i=0;i<nb;i++){
					stacks.get(end).push(tempo.pop());
				}

			}
		}

		String result= "";
		for (Map.Entry<Integer, Stack> entry : stacks.entrySet()) {
			result+= entry.getValue().firstElement();
		}
		System.out.println(result);

	}}
