package day;

import Utils.Position;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Day10 {

	public static void main(String[] args) throws FileNotFoundException {

		Scanner sc = new Scanner(new File("src/sources/day10.txt"));
		int cycle = 1;
		Map<Integer,Integer> exec = new HashMap<Integer,Integer>();
		Map<Integer,Integer> registre = new HashMap<Integer,Integer>();
		String sprite = "";

		int X = 1;

		while (sc.hasNextLine()) {
			String line = sc.nextLine();
			if(!line.contains("noop")){
				Integer V = Integer.valueOf(line.split(" ")[1]);
				exec.put(cycle,V);
				registre.put(cycle,X);
				sprite += sprite(cycle,X);
				cycle++;
			}

			registre.put(cycle,X);
			sprite += sprite(cycle,X);
			if(exec.get(cycle-1) != null){
				X+= exec.get(cycle-1);
			}
			cycle++;

		}

		System.out.println((registre.get(20)*20)+(registre.get(60)*60)+(registre.get(100)*100)+(registre.get(140)*140)+(registre.get(180)*180)+(registre.get(220)*220));
		System.out.println(sprite.substring(0,40));
		System.out.println(sprite.substring(40,80));
		System.out.println(sprite.substring(80,120));
		System.out.println(sprite.substring(120,160));
		System.out.println(sprite.substring(160,200));
		System.out.println(sprite.substring(200,240));

	}

	private static String sprite(int cycle, int X) {
		String sprite;
		if(X <= (cycle%40) && (cycle%40) <= X+2) {
			sprite = "#";
		}else {
			sprite = ".";
		}
		return sprite;
	}
}