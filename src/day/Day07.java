package day;

import Utils.Contenu;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

class Day07 {

	public static void main(String[] args) throws FileNotFoundException {

		Scanner sc = new Scanner(new File("src/sources/day07.txt"));
		Contenu racine = new Contenu();
		racine.name = "/";
		sc.nextLine();
		Contenu encours = racine;

		while (sc.hasNextLine()) {
			encours = recursive(sc, encours);
		}

		for (Map.Entry<String, Contenu> entry : racine.contenuDescendant.entrySet()) {
			racine.taille += entry.getValue().taille;
		}
		List<Contenu> aVoir = new ArrayList<Contenu>();
		aVoir.add(racine);
		aVoir.addAll(test(racine));

		int retour1 = 0;
		for (Contenu dossier : aVoir) {
			if (dossier.taille < 100001) {
				retour1 += dossier.taille;
			}
		}

		int retour2 = Integer.MAX_VALUE;
		int minimum = 30000000 - (70000000 - racine.taille);
		List<Contenu> aGarder = new ArrayList<Contenu>();

		for (Contenu dossier : aVoir) {
			if (dossier.taille >= minimum) {
				if (dossier.taille < retour2) {
					retour2 = dossier.taille;
				}
			}
		}

		System.out.println(retour1);
		System.out.println(retour2);
	}

	private static List<Contenu> test(Contenu descend) {
		List<Contenu> retour = new ArrayList<Contenu>();
		if (descend.contenuDescendant != null) {
			for (Map.Entry<String, Contenu> entry : descend.contenuDescendant.entrySet()) {
				if (entry.getValue().name.contains("dir ")) {
					retour.add(entry.getValue());
					retour.addAll(test(entry.getValue()));
				}
			}
		}

		return retour;
	}


	private static Contenu recursive(Scanner sc, Contenu racine) {
		String line = sc.nextLine();
		if (line.contains("dir")) {
			Contenu dossier = new Contenu();
			dossier.name = line;
			dossier.contenuAscendant = racine;
			racine.contenuDescendant.put(dossier.name, dossier);
		} else if (line.contains("$ ls")) {
		} else if (line.contains("$ cd")) {
			if (line.contains("..")) {
				racine = racine.contenuAscendant;
			} else {
				racine = racine.contenuDescendant.get("dir " + line.split(" ")[2]);
			}

		} else {
			Contenu fichier = new Contenu();
			fichier.name = line.split(" ")[1];
			fichier.taille = Integer.valueOf(line.split(" ")[0]);
			fichier.contenuAscendant = racine;
			racine.contenuDescendant.put(fichier.name, fichier);
			Contenu remonte = racine;
			while (remonte.contenuAscendant != null) {
				remonte.taille += fichier.taille;
				remonte = remonte.contenuAscendant;
			}
		}

		return racine;
	}
}