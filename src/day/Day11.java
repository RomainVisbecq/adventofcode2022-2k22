package day;

import Utils.Monkey;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.*;

public class Day11 {

	public static void main(String[] args) throws FileNotFoundException {

		Scanner sc = new Scanner(new File("src/sources/day11.txt"));
		Map<Integer, Monkey> monkeys = new HashMap<Integer,Monkey>();
		int round = 10000;
		long nb2 =0;
		long ppcm = 0;

		while (sc.hasNextLine()) {
			String line = sc.nextLine();
			String[] monkeynb = line.split(" ")[1].split(":");
			Monkey monkey = new Monkey();
			//items
			line = sc.nextLine();
			String[] itemssplit = line.split(": ")[1].split(", ");
			for(String item : itemssplit){
				monkey.itemsLong.add(Long.valueOf(item));
			}

			//operation
			line =sc.nextLine();
			String[] operationsplit = line.split(":")[1].split("= ");
			monkey.operation = operationsplit[1];

			//test
			line =sc.nextLine();
			String[] testsplit = line.split(":")[1].split("by ");
			monkey.test = Integer.valueOf(testsplit[1]);
			if(nb2 == 0){
				ppcm = ppcm(monkey.test,1);
				nb2 = monkey.test;
			}else{
				long temp = ppcm;
				ppcm = ppcm(temp, monkey.test);
			}


			//trueValue
			line =sc.nextLine();
			String[] truesplit = line.split(":")[1].split("throw to monkey ");
			monkey.trueValue = Integer.valueOf(truesplit[1]);

			//falseValue
			line =sc.nextLine();
			String[] falseValue = line.split(":")[1].split("throw to monkey ");
			monkey.falseValue = Integer.valueOf(falseValue[1]);

			monkeys.put(Integer.valueOf(Integer.valueOf(monkeynb[0])),monkey);

			if(sc.hasNextLine()){
				sc.nextLine();
			}
		}

		for(int i=1;i<round+1;i++){
			for (Map.Entry<Integer, Monkey> monkeyEntry : monkeys.entrySet()) {
				Monkey monkey = monkeyEntry.getValue();
				List<Long> itemsRemove = new ArrayList<Long>();
				for(long item :monkey.itemsLong){
					long worry;
					String[] operations = monkey.operation.replaceAll(" ","").replaceAll("old",String.valueOf(item)).split("\\*");
					if(operations.length>1){
						worry = (Long.valueOf(operations[0])*Long.valueOf(operations[1]));
					}else{
						operations = monkey.operation.replaceAll(" ","").replaceAll("old",String.valueOf(item)).split("\\+");
						worry = (Long.valueOf(operations[0])+Long.valueOf(operations[1]));
					}

					worry = worry%ppcm;

					if (worry%ppcm%monkey.test == 0) {
						monkeys.get(monkey.trueValue).itemsLong.add(worry);
					}else{
						monkeys.get(monkey.falseValue).itemsLong.add(worry);
					}
					itemsRemove.add(item);
					monkey.inspect++;
				}
				monkey.itemsLong.removeAll(itemsRemove);
			}
/*
			System.out.println("After round "+i);
			for (Map.Entry<Integer, Monkey> monkeyEntry : monkeys.entrySet()) {
				System.out.println("Monkey " + monkeyEntry.getKey() + " : "+ monkeyEntry.getValue().items);
				System.out.println("Monkey " + monkeyEntry.getKey() + " inspected items : "+ monkeyEntry.getValue().inspect);

			}
			System.out.println("\n");
*/
		}

		System.out.println("After round "+round);
		for (Map.Entry<Integer, Monkey> monkeyEntry : monkeys.entrySet()) {
			System.out.println("Monkey " + monkeyEntry.getKey() + " : "+ monkeyEntry.getValue().itemsLong);
			System.out.println("Monkey " + monkeyEntry.getKey() + " inspected items : "+ monkeyEntry.getValue().inspect);

		}
		System.out.println("\n");

		long max1 = 0;
		long max2 = 0;
		for (Map.Entry<Integer, Monkey> monkeyEntry : monkeys.entrySet()) {
			Monkey monk = monkeyEntry.getValue();
			if(monk.inspect > max1){
				max2 = max1;
				max1 = monk.inspect;
			}else if(monk.inspect > max2){
				max2 = monk.inspect;
			}
		}
		System.out.println(max1*max2);


	}
	static long ppcm(long a, long b){
		long p=a*b;
		while (a!=b) if (a<b) b-=a; else a-=b;
		return p/a;
	}
}