package day;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.Array;
import java.util.*;

public class Day06 {

	public static void main(String[] args) throws FileNotFoundException {

		Scanner sc = new Scanner(new File("src/sources/day06.txt"));
		int count1 = 0;
		String line = sc.nextLine();

		ArrayList<Character> marqueur = new ArrayList<Character>();
		int t= 4;

		for (int i=0;i<line.length();i++) {
			char caract = line.charAt(i);
			if (marqueur.size() != t) {
				marqueur.add(caract);
			} else {
				Set<Character> test = new HashSet<>(marqueur);
				if (test.size() == marqueur.size()) {
					System.out.println(i);
					break;
				} else {
					marqueur.remove(marqueur.get(0));
					marqueur.add(caract);
				}
			}
		}

		ArrayList<Character> message = new ArrayList<Character>();
		for (int i=0;i<line.length();i++) {
			char caract = line.charAt(i);
			if(message.size()!=t){
				message.add(caract);
			}else{
				Set<Character> test = new HashSet<>(message);
				if(test.size()==message.size()){
					System.out.println(i);
					break;
				}else{
					message.remove(message.get(0));
					message.add(caract);
				}
			}

		}


	}
}