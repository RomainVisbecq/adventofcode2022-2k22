package day;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Day03{

	final static String alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

	public static void main(String[] args) throws FileNotFoundException {

		Scanner sc = new Scanner(new File("src/sources/day03.txt"));
		ArrayList<String> lines = new ArrayList<String>();
		while (sc.hasNextLine()) {
			String line = sc.nextLine();
			lines.add(line);
		}

		System.out.println(partOne(lines));
		System.out.println(partTwo(lines));

	}


	static int partOne(ArrayList<String> lines) {
		ArrayList<Character> totalPriority = new ArrayList<Character>();

		for(String line : lines) {
			String partOne = line.substring(0, line.length() / 2);
			String partTwo = line.substring(line.length() / 2, line.length());

			for (char lettre : partOne.toCharArray()) {
				String lettreS = String.valueOf(lettre);
				if (partTwo.contains(lettreS)) {
					totalPriority.add(lettreS.charAt(0));
					break;
				}
			}
		}
		int response = 0;
		for(Character lettre : totalPriority){
			int retour = alphabet.indexOf((lettre))+1;
			response += retour;
			}
		return response;
	}

	static int partTwo(ArrayList<String> lines) {
		ArrayList<Character> totalPriority = new ArrayList<Character>();

		for(int i=0;i<lines.size();i=i+3) {
			String partOne = lines.get(i);
			String partTwo = lines.get(i+1);
			String partThree = lines.get(i+2);

			for (char lettre : partOne.toCharArray()) {
				String lettreS = String.valueOf(lettre);
				if (partTwo.contains(lettreS) && partThree.contains(lettreS)) {
					totalPriority.add(lettreS.charAt(0));
					break;
				}
			}
		}
		int response = 0;
		for(Character lettre : totalPriority){
			int retour = alphabet.indexOf((lettre))+1;
			response += retour;
		}
		return response;
	}
}
