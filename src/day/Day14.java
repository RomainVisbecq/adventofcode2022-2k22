package day;

import Utils.Pair;
import Utils.Position;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Day14 {

	public static void main(String[] args) throws FileNotFoundException {

		Scanner sc = new Scanner(new File("src/sources/day14.txt"));
		Map<String,String> sol = new HashMap<>();

		int maxY = Integer.MIN_VALUE;
		int minX = Integer.MAX_VALUE;
		int maxX = Integer.MIN_VALUE;

		while (sc.hasNextLine()) {
			String[] roches = sc.nextLine().split(" -> ");
			for(int i=1;i<roches.length;i++){
				String[] rocheD = roches[i].split(",");
				String[] rocheA = roches[i-1].split(",");
				int xD = Integer.valueOf(rocheD[0]);
				int yD = Integer.valueOf(rocheD[1]);
				int xA = Integer.valueOf(rocheA[0]);
				int yA = Integer.valueOf(rocheA[1]);

				if(xA == xD){
					if(yA < yD){
						while(yA <= yD){
							if(sol.get(xA+","+yA) == null){
								sol.put(xA+","+yA,"#");
								if(maxY<yA){
									maxY = yA;
								}
								if(minX>xA){
									minX=xA;
								}
								if(maxX<xA){
									maxX=xA;
								}
							}
							yA++;
						}
					}else{
						while(yD <= yA){
							if(sol.get(xD+","+yD) == null){
								sol.put(xD+","+yD,"#");
								if(maxY<yD){
									maxY = yD;
								}
								if(minX>xD){
									minX=xD;
								}
								if(maxX<xD){
									maxX=xD;
								}
							}
							yD++;
						}
					}
				}else{
					if(xA < xD){
						while(xA <= xD){
							if(sol.get(xA+","+yA) == null){
								sol.put(xA+","+yA,"#");
								if(maxY<yA){
									maxY = yA;
								}
								if(minX>xA){
									minX=xA;
								}
								if(maxX<xA){
									maxX=xA;
								}
							}
							xA++;
						}
					}else{
						while(xD <= xA){
							if(sol.get(xD+","+yD) == null){
								sol.put(xD+","+yD,"#");
								if(maxY<yD){
									maxY = yD;
								}
								if(minX>xD){
									minX=xD;
								}
								if(maxX<xD){
									maxX=xD;
								}
							}
							xD++;
						}
					}
				}
			}
		}

//partTwo, if partOne => comment
		maxY +=2;
		minX -= maxY*2;
		maxX += maxY*2;
		int itMinX = minX;

		while(itMinX <= maxX){
			if(sol.get(itMinX+","+maxY) == null){
				sol.put(itMinX+","+maxY,"#");
			}
			itMinX++;
		}
//partTwo, if partOne => comment

		Position enCours = new Position();
		enCours.x = 500;
		enCours.y = 0;
		Position lastEnCours = new Position();
		lastEnCours.x = maxX;
		lastEnCours.y = maxY;

		int countsable = 0;

		while((lastEnCours.x != 500 || lastEnCours.y != 0) && enCours.y <= maxY) {
				if (sol.get(enCours.x+ "," + (enCours.y+1)) == null) {
					enCours.y++;
				} else {
					if (sol.get((enCours.x-1) + "," + (enCours.y+1)) == null) {
						enCours.x--;
						enCours.y++;
					} else {
						if (sol.get((enCours.x+1) + "," + (enCours.y+1)) == null) {
							enCours.x++;
							enCours.y++;
						} else {
							sol.put(enCours.x + "," + enCours.y, "o");
							countsable++;
							lastEnCours.x = enCours.x;
							lastEnCours.y = enCours.y;
							enCours.x = 500;
							enCours.y = 0;
						}
					}
				}
			}

		System.out.println(countsable);

	}
}