package day;

import Utils.Pair;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Day04 {

	public static void main(String[] args) throws FileNotFoundException {

		Scanner sc = new Scanner(new File("src/sources/day04.txt"));
		int count1 = 0;
		int count2 = 0;
		while (sc.hasNextLine()) {
			String line = sc.nextLine();
			String[] pairs = line.split(",");
			Pair pair1 = new Pair(Integer.valueOf(pairs[0].split("-")[0]),Integer.valueOf(pairs[0].split("-")[1]));
			Pair pair2 = new Pair(Integer.valueOf(pairs[1].split("-")[0]),Integer.valueOf(pairs[1].split("-")[1]));

			if((pair1.partOne<=pair2.partOne && pair1.partTwo>=pair2.partTwo) ||
					(pair2.partOne<=pair1.partOne && pair2.partTwo>=pair1.partTwo)){
				count1++;
			}
			if((pair1.partOne<=pair2.partOne && pair1.partTwo>=pair2.partOne) ||
					(pair2.partOne<=pair1.partOne && pair2.partTwo>=pair1.partOne)){
				count2++;
			}
		}

		System.out.println(count1);
		System.out.println(count2);

	}}
