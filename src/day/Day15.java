package day;

import Utils.Position;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.*;

public class Day15 {

	public static void main(String[] args) throws FileNotFoundException {

		Scanner sc = new Scanner(new File("src/sources/day15.txt"));
		Map<String,Integer> capteurs = new HashMap<>();
		int yRecherche = 4000000;

		while (sc.hasNextLine()) {
			String[] line = sc.nextLine().split(": closest beacon is at ");
			String[] capteur = line[0].split("Sensor at ")[1].split(",");
			int capteurX = Integer.valueOf(capteur[0].split("=")[1]);
			int capteurY = Integer.valueOf(capteur[1].split("=")[1]);
			String[] balise = line[1].split(",");
			int baliseX = Integer.valueOf(balise[0].split("=")[1]);
			int baliseY = Integer.valueOf(balise[1].split("=")[1]);
			int manhattan = Math.abs(capteurX-baliseX)+Math.abs(capteurY-baliseY);
			capteurs.put(capteurX+","+capteurY,manhattan);
			capteurs.put(baliseX+","+baliseY,-1);

		}

		for(int i=0;i<=yRecherche;i++) {
			Map<String,String> noBalises = new HashMap<>();
			List<String> intervalles = new ArrayList<>();

			for (Map.Entry<String, Integer> addCapteurs : capteurs.entrySet()) {
				int xA = Integer.valueOf(addCapteurs.getKey().split(",")[0]);
				int yA = Integer.valueOf(addCapteurs.getKey().split(",")[1]);
				if (yA == i) {
					noBalises.put(xA + "," + yA, "");
					intervalles.add(xA+","+xA);
				}
			}
			for (Map.Entry<String, Integer> capteur : capteurs.entrySet()) {
				int distance = capteur.getValue();
				int x = Integer.valueOf(capteur.getKey().split(",")[0]);
				int y = Integer.valueOf(capteur.getKey().split(",")[1]);
				int diff = Math.abs(i - y);

				if (capteur.getValue() != -1 && distance >= diff) {
					int count = 0;
					int xMin = x-(distance-diff) > 0 ? x-(distance-diff) : 0;
					int xMax = x+(distance-diff) < yRecherche ? x+(distance-diff) : yRecherche;
					intervalles.add(xMin+","+xMax);

					/*while (count <= (distance - diff) && noBalises.size() <= yRecherche) {
						if (noBalises.get(x + count + "," + i) == null && (x + count) >= 0 && (x + count) <= yRecherche) {
							noBalises.put(x + count + "," + i, "#");
						}
						if (noBalises.get(x - count + "," + i) == null && (x - count) >= 0 && (x - count) <= yRecherche) {
							noBalises.put(x - count + "," + i, "#");
						}
						count++;
					} */
				}
			}

			Collections.sort(intervalles);

			int minI = Integer.valueOf(intervalles.get(0).split(",")[0]) < 0 ? 0 : Integer.valueOf(intervalles.get(0).split(",")[0]) > yRecherche ? yRecherche : Integer.valueOf(intervalles.get(0).split(",")[0]);
			int maxI = Integer.valueOf(intervalles.get(0).split(",")[1]) < 0 ? 0 : Integer.valueOf(intervalles.get(0).split(",")[1]) > yRecherche ? yRecherche : Integer.valueOf(intervalles.get(0).split(",")[1]);
			int test = 0;
			while((minI != 0 || maxI != yRecherche) && test <= intervalles.size()){
				for(int r=1;r<intervalles.size();r++){
					int xD = Integer.valueOf(intervalles.get(r).split(",")[0]);
					int xA = Integer.valueOf(intervalles.get(r).split(",")[1]);
					if(xD >= minI && xD <= maxI){
						if(xA > maxI){
							maxI = xA;
						}
					}else if(xA >= minI && xA <= maxI){
						if(xD < minI){
							minI = xD;
						}
					}else if (xD == maxI+1){
						maxI = xA;
					}else if (xA == minI-1){
						minI = xD;
					}
				}
				test++;
			}

			if(minI != 0 || maxI != yRecherche){
				if(minI != 0){
					System.out.println(new BigDecimal(minI).multiply(new BigDecimal(4000000)).add(new BigDecimal(i)));
				}else{
					System.out.println(new BigDecimal(maxI).multiply(new BigDecimal(4000000)).add(new BigDecimal(i)));
				}
			}
		}
	}
}