package day;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.stream.Collectors;

public class Day02 {

	public static void main(String[] args) throws FileNotFoundException {

		Scanner sc = new Scanner(new File("src/sources/day02.txt"));
		int  score1 = 0;
		int  score2 = 0;

		Map<String,Integer> combinaisons1 = new HashMap<String,Integer>();
		Map<String,Integer> combinaisons2 = new HashMap<String,Integer>();

		combinaisons1.put("A X",new Integer(4));
		combinaisons1.put("A Y",new Integer(8));
		combinaisons1.put("A Z",new Integer(3));
		combinaisons1.put("B X",new Integer(1));
		combinaisons1.put("B Y",new Integer(5));
		combinaisons1.put("B Z",new Integer(9));
		combinaisons1.put("C X",new Integer(7));
		combinaisons1.put("C Y",new Integer(2));
		combinaisons1.put("C Z",new Integer(6));

		combinaisons2.put("A X",new Integer(3));
		combinaisons2.put("A Y",new Integer(4));
		combinaisons2.put("A Z",new Integer(8));
		combinaisons2.put("B X",new Integer(1));
		combinaisons2.put("B Y",new Integer(5));
		combinaisons2.put("B Z",new Integer(9));
		combinaisons2.put("C X",new Integer(2));
		combinaisons2.put("C Y",new Integer(6));
		combinaisons2.put("C Z",new Integer(7));

		while (sc.hasNextLine()) {
			String line = sc.nextLine();
			score1 += combinaisons1.get(line);
			score2 += combinaisons2.get(line);
		}

		System.out.println(score1);
		System.out.println(score2);

	}
}
