package Utils;

public class Pair{
	public int partOne = 0;
	public int partTwo = 0;

	public String lineOne = "";
	public String lineTwo = "";

	public Pair(){
	}
	public Pair(int partOne,int partTwo){
		this.partOne=partOne;
		this.partTwo=partTwo;
	}

}
